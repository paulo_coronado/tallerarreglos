
//Arreglo sin elementos
let miArreglo=[];

// push() 

for(let i=0;i<50;i++){

    let numero;
    numero=Math.round(Math.random()*499+1);
    miArreglo.push(numero);
}

//Imprimir impares

for(let i=0;i<50;i++){

    if(miArreglo[i]%2!=0){
        document.write(miArreglo[i]);
        document.write("<br>");
    }
    
}

console.log(miArreglo);

//Imprimir los números que sean primos

for(let i=0;i<50;i++){
    let r;
    r=esPrimo(miArreglo[i]);
    
    if(r===true){
        document.write(miArreglo[i]);
        document.write("<br>");
    }
}

//console.log(miArreglo);


function esPrimo(valor){

    let m=valor;
    let n;
    let r;
    let primo = true;
    
    for (n = 2; n < m; n++) {
      r = m % n;
      if (r === 0) {
        primo = false;
      }
    }
    
    return primo;
}
